#include <vector>

template<typename T>
void swap(std::vector<T> &list, size_t i1, size_t i2) {
    T temp = list[i1];
    list[i1] = list[i2];
    list[i2] = temp;
}

template<typename T>
void selection_sort(std::vector<T> &list) {
    size_t min;
    for (size_t i = 0; i < list.size(); i++) {
        min = i;

        for (size_t j = i; j < list.size(); j++) {
            if (list[j] < list[min]) {
                min = j;
            }
        }

        swap(list, i, min);
    }
}
