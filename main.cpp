#include <iostream>
#include "sort.hpp"

template<typename T>
void get_vector_input(std::vector<T> &vec, T &terminator) {
  T input_value;

  while (input_value != terminator) {
    if (std::is_same<T, std::string>::value) {
        std::getline(std::cin, input_value);
    } else {
        std::cin >> input_value;
    }
    vec.push_back(input_value);
  }

  vec.pop_back(); // remove the terminator
}

template<typename T>
void print_vector(std::vector<T> &vec) {
    std::cout << '[';
    
    size_t i;
    for (i = 0; i < vec.size() - 1; i++) {
        std::cout << vec[i] << ", ";
    }
    
    std::cout << vec[i] << ']' << std::endl; 
}

int main() {
    std::vector<std::string> input_values;
    std::string terminator = "/end";
  
    std::cout << "Hey! Please input some values (\""
	      << terminator
	      << "\" to stop):"
	      << std::endl;

    get_vector_input(input_values, terminator);

    std::cout << "Your values: ";
    print_vector(input_values);

    selection_sort(input_values);
  
    std::cout << "Your values after sorting: ";
    print_vector(input_values);
}

